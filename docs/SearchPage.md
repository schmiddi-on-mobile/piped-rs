# SearchPage

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**corrected** | Option<**bool**> | Whether the search query was corrected. | [optional]
**items** | Option<[**Vec<models::SearchItem>**](SearchItem.md)> |  | [optional]
**nextpage** | Option<**String**> | The parameter used to get the next page of this page. | [optional]
**suggestion** | Option<**String**> | The suggested search query. | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


