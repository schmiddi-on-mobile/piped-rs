# Subtitle

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**auto_generated** | Option<**bool**> | Whether the subtitle is auto generated. | [optional]
**code** | Option<**String**> | The language code of the subtitle. | [optional]
**mime_type** | Option<**String**> | The mime type of the subtitle. | [optional]
**url** | Option<**String**> | The URL of the subtitle. | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


