# ChannelItem

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**r#type** | Option<**String**> | The type of the channel item. Always channel. | [optional]
**description** | Option<**String**> | The description of the channel. | [optional]
**name** | Option<**String**> | The name of the channel. | [optional]
**subscribers** | Option<**i32**> | The number of subscribers the channel has. | [optional]
**thumbnail** | Option<**String**> | The thumbnail of the channel. | [optional]
**url** | Option<**String**> | The relative URL of the channel. | [optional]
**verified** | Option<**bool**> | Whether the channel is verified. | [optional]
**videos** | Option<**i32**> | The number of videos the channel has. | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


