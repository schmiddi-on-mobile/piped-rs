# Comment

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**author** | Option<**String**> | The name of the author of the comment. | [optional]
**thumbnail** | Option<**String**> | The thumbnail/avatar of the author of the comment. | [optional]
**comment_id** | Option<**String**> | The ID of the comment. | [optional]
**comment_text** | Option<**String**> | The text of the comment. | [optional]
**commented_time** | Option<**String**> | The relative time the comment was made. | [optional]
**commentor_url** | Option<**String**> | The relative URL of the author of the comment. | [optional]
**replies_page** | Option<**String**> | The parameter used as the nextpage to fetch replies for this comment. | [optional]
**like_count** | Option<**i32**> | The number of likes the comment has. | [optional]
**reply_count** | Option<**i32**> | The number of replies the comment has. | [optional]
**hearted** | Option<**bool**> | Whether the comment was hearted by the video's uploader. | [optional]
**pinned** | Option<**bool**> | Whether the comment was pinned by the video's uploader. | [optional]
**verified** | Option<**bool**> | Whether the author of the comment is verified. | [optional]
**creator_replied** | Option<**bool**> | Whether the video's uploader replied to the comment. | [optional]
**channel_owner** | Option<**bool**> | Whether the author of the comment is the video's uploader. | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


