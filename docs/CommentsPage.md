# CommentsPage

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**comments** | Option<[**Vec<models::Comment>**](Comment.md)> |  | [optional]
**nextpage** | Option<**String**> | The parameter used to get the next page of comments. | [optional]
**disabled** | Option<**bool**> | Whether or not comments are disabled on the video. | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


