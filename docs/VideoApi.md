# \VideoApi

All URIs are relative to *https://pipedapi.kavin.rocks*

Method | HTTP request | Description
------------- | ------------- | -------------
[**comments**](VideoApi.md#comments) | **GET** /comments/{videoId} | Gets Comments
[**comments_next_page**](VideoApi.md#comments_next_page) | **GET** /nextpage/comments/{videoId} | Gets more comments
[**stream_info**](VideoApi.md#stream_info) | **GET** /streams/{videoId} | Gets Video Information



## comments

> models::CommentsPage comments(video_id)
Gets Comments

Gets the comments for a video. 

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**video_id** | **String** | The video ID of the YouTube video you want to get comments from. | [required] |

### Return type

[**models::CommentsPage**](CommentsPage.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## comments_next_page

> models::CommentsPage comments_next_page(video_id, nextpage)
Gets more comments

Gets more comments. 

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**video_id** | **String** | The video ID of the YouTube video you want to get more comments from. | [required] |
**nextpage** | **String** | The next page token to get more comments from. | [required] |

### Return type

[**models::CommentsPage**](CommentsPage.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## stream_info

> models::VideoInfo stream_info(video_id)
Gets Video Information

Gets all available Stream information about a video. 

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**video_id** | **String** | The video ID of the YouTube video you want to get information about. | [required] |

### Return type

[**models::VideoInfo**](VideoInfo.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

