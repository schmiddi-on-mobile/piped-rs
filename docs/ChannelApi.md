# \ChannelApi

All URIs are relative to *https://pipedapi.kavin.rocks*

Method | HTTP request | Description
------------- | ------------- | -------------
[**channel_info_id**](ChannelApi.md#channel_info_id) | **GET** /channel/{channelId} | Gets Channel Information from ID.
[**channel_info_name**](ChannelApi.md#channel_info_name) | **GET** /c/{name} | Gets Channel Information from name.
[**channel_info_username**](ChannelApi.md#channel_info_username) | **GET** /user/{username} | Gets Channel Information from username.
[**channel_next_page**](ChannelApi.md#channel_next_page) | **GET** /nextpage/channel/{channelId} | Gets more channel videos



## channel_info_id

> models::ChannelInfo channel_info_id(channel_id)
Gets Channel Information from ID.

Gets all available Channel information about a channel. 

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**channel_id** | **String** | The channel ID of the YouTube channel you want to get information about. | [required] |

### Return type

[**models::ChannelInfo**](ChannelInfo.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## channel_info_name

> models::ChannelInfo channel_info_name(name)
Gets Channel Information from name.

Gets all available Channel information about a channel. 

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**name** | **String** | The name of the YouTube channel you want to get information about. | [required] |

### Return type

[**models::ChannelInfo**](ChannelInfo.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## channel_info_username

> models::ChannelInfo channel_info_username(username)
Gets Channel Information from username.

Gets all available Channel information about a channel. 

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**username** | **String** | The username of the YouTube channel you want to get information about. | [required] |

### Return type

[**models::ChannelInfo**](ChannelInfo.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## channel_next_page

> models::StreamsPage channel_next_page(channel_id, nextpage)
Gets more channel videos

Gets more channel videos. 

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**channel_id** | **String** | The channel ID of the YouTube channel you want to get more videos from. | [required] |
**nextpage** | **String** | The next page token to get more videos from. | [required] |

### Return type

[**models::StreamsPage**](StreamsPage.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

