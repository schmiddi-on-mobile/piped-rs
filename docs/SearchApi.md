# \SearchApi

All URIs are relative to *https://pipedapi.kavin.rocks*

Method | HTTP request | Description
------------- | ------------- | -------------
[**search**](SearchApi.md#search) | **GET** /search | Searches for videos, channels, and playlists.
[**search_next_page**](SearchApi.md#search_next_page) | **GET** /nextpage/search | Gets more search results



## search

> models::SearchPage search(q, filter)
Searches for videos, channels, and playlists.

Searches for videos, channels, and playlists. 

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**q** | **String** | The search query string. | [required] |
**filter** | [**SearchFilter**](.md) | The filter parameter specifies a filter query that restricts the results to items that match the filter. | [required] |

### Return type

[**models::SearchPage**](SearchPage.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## search_next_page

> models::SearchPage search_next_page(nextpage, q, filter)
Gets more search results

Gets more search results. 

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**nextpage** | **String** | The next page token to get more search results from. | [required] |
**q** | **String** | The search query string. | [required] |
**filter** | [**SearchFilter**](.md) | The filter parameter specifies a filter query that restricts the results to items that match the filter. | [required] |

### Return type

[**models::SearchPage**](SearchPage.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

