# \FeedApi

All URIs are relative to *https://pipedapi.kavin.rocks*

Method | HTTP request | Description
------------- | ------------- | -------------
[**feed_unauthenticated**](FeedApi.md#feed_unauthenticated) | **GET** /feed/unauthenticated | Generate a feed while unauthenticated, from a list of channelIds.



## feed_unauthenticated

> Vec<models::StreamItem> feed_unauthenticated(channels)
Generate a feed while unauthenticated, from a list of channelIds.

Generates a user feed while unauthenticated. 

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**channels** | [**Vec<String>**](String.md) | A list of channelIds to generate a feed from. | [required] |

### Return type

[**Vec<models::StreamItem>**](StreamItem.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

