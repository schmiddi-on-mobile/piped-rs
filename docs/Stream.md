# Stream

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**itag** | Option<**i32**> | The itag of the stream, or -1 if not available. | [optional]
**url** | Option<**String**> | The URL of the stream. | [optional]
**format** | Option<**String**> | The format of the stream. | [optional]
**quality** | Option<**String**> | The quality of the stream. | [optional]
**mime_type** | Option<**String**> | The mime type of the stream. | [optional]
**codec** | Option<**String**> | The codec of the stream. | [optional]
**video_only** | Option<**bool**> |  | [optional]
**bitrate** | Option<**i32**> |  | [optional]
**init_start** | Option<**i32**> |  | [optional]
**init_end** | Option<**i32**> |  | [optional]
**index_start** | Option<**i32**> |  | [optional]
**index_end** | Option<**i32**> |  | [optional]
**width** | Option<**i32**> |  | [optional]
**height** | Option<**i32**> |  | [optional]
**fps** | Option<**i32**> |  | [optional]
**audio_track_id** | Option<**String**> |  | [optional]
**audio_track_name** | Option<**String**> |  | [optional]
**audio_track_type** | Option<**String**> |  | [optional]
**audio_track_locale** | Option<**String**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


