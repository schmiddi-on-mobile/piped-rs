# StreamsPage

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**related_streams** | Option<[**Vec<models::StreamItem>**](StreamItem.md)> |  | [optional]
**nextpage** | Option<**String**> | The parameter used to get the next page of this page. | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


