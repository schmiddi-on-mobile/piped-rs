# ChannelInfo

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**avatar_url** | Option<**String**> | The URL of the channel's avatar. | [optional]
**banner_url** | Option<**String**> | The URL of the channel's banner. | [optional]
**description** | Option<**String**> | The channel's description. | [optional]
**id** | Option<**String**> | The ID of the channel. | [optional]
**name** | Option<**String**> | The name of the channel. | [optional]
**nextpage** | Option<**String**> | The parameter used to get the next page of related videos. | [optional]
**related_streams** | Option<[**Vec<models::StreamItem>**](StreamItem.md)> |  | [optional]
**subscriber_count** | Option<**i32**> | The number of subscribers the channel has. | [optional]
**verified** | Option<**bool**> | Whether the channel is verified. | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


