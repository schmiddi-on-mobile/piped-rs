# \UnauthenticatedApi

All URIs are relative to *https://pipedapi.kavin.rocks*

Method | HTTP request | Description
------------- | ------------- | -------------
[**channel_info_id**](UnauthenticatedApi.md#channel_info_id) | **GET** /channel/{channelId} | Gets Channel Information from ID.
[**channel_info_name**](UnauthenticatedApi.md#channel_info_name) | **GET** /c/{name} | Gets Channel Information from name.
[**channel_info_username**](UnauthenticatedApi.md#channel_info_username) | **GET** /user/{username} | Gets Channel Information from username.
[**channel_next_page**](UnauthenticatedApi.md#channel_next_page) | **GET** /nextpage/channel/{channelId} | Gets more channel videos
[**comments**](UnauthenticatedApi.md#comments) | **GET** /comments/{videoId} | Gets Comments
[**comments_next_page**](UnauthenticatedApi.md#comments_next_page) | **GET** /nextpage/comments/{videoId} | Gets more comments
[**feed_unauthenticated**](UnauthenticatedApi.md#feed_unauthenticated) | **GET** /feed/unauthenticated | Generate a feed while unauthenticated, from a list of channelIds.
[**search**](UnauthenticatedApi.md#search) | **GET** /search | Searches for videos, channels, and playlists.
[**search_next_page**](UnauthenticatedApi.md#search_next_page) | **GET** /nextpage/search | Gets more search results
[**stream_info**](UnauthenticatedApi.md#stream_info) | **GET** /streams/{videoId} | Gets Video Information
[**trending**](UnauthenticatedApi.md#trending) | **GET** /trending | Gets all Trending Videos



## channel_info_id

> models::ChannelInfo channel_info_id(channel_id)
Gets Channel Information from ID.

Gets all available Channel information about a channel. 

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**channel_id** | **String** | The channel ID of the YouTube channel you want to get information about. | [required] |

### Return type

[**models::ChannelInfo**](ChannelInfo.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## channel_info_name

> models::ChannelInfo channel_info_name(name)
Gets Channel Information from name.

Gets all available Channel information about a channel. 

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**name** | **String** | The name of the YouTube channel you want to get information about. | [required] |

### Return type

[**models::ChannelInfo**](ChannelInfo.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## channel_info_username

> models::ChannelInfo channel_info_username(username)
Gets Channel Information from username.

Gets all available Channel information about a channel. 

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**username** | **String** | The username of the YouTube channel you want to get information about. | [required] |

### Return type

[**models::ChannelInfo**](ChannelInfo.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## channel_next_page

> models::StreamsPage channel_next_page(channel_id, nextpage)
Gets more channel videos

Gets more channel videos. 

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**channel_id** | **String** | The channel ID of the YouTube channel you want to get more videos from. | [required] |
**nextpage** | **String** | The next page token to get more videos from. | [required] |

### Return type

[**models::StreamsPage**](StreamsPage.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## comments

> models::CommentsPage comments(video_id)
Gets Comments

Gets the comments for a video. 

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**video_id** | **String** | The video ID of the YouTube video you want to get comments from. | [required] |

### Return type

[**models::CommentsPage**](CommentsPage.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## comments_next_page

> models::CommentsPage comments_next_page(video_id, nextpage)
Gets more comments

Gets more comments. 

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**video_id** | **String** | The video ID of the YouTube video you want to get more comments from. | [required] |
**nextpage** | **String** | The next page token to get more comments from. | [required] |

### Return type

[**models::CommentsPage**](CommentsPage.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## feed_unauthenticated

> Vec<models::StreamItem> feed_unauthenticated(channels)
Generate a feed while unauthenticated, from a list of channelIds.

Generates a user feed while unauthenticated. 

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**channels** | [**Vec<String>**](String.md) | A list of channelIds to generate a feed from. | [required] |

### Return type

[**Vec<models::StreamItem>**](StreamItem.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## search

> models::SearchPage search(q, filter)
Searches for videos, channels, and playlists.

Searches for videos, channels, and playlists. 

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**q** | **String** | The search query string. | [required] |
**filter** | [**SearchFilter**](.md) | The filter parameter specifies a filter query that restricts the results to items that match the filter. | [required] |

### Return type

[**models::SearchPage**](SearchPage.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## search_next_page

> models::SearchPage search_next_page(nextpage, q, filter)
Gets more search results

Gets more search results. 

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**nextpage** | **String** | The next page token to get more search results from. | [required] |
**q** | **String** | The search query string. | [required] |
**filter** | [**SearchFilter**](.md) | The filter parameter specifies a filter query that restricts the results to items that match the filter. | [required] |

### Return type

[**models::SearchPage**](SearchPage.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## stream_info

> models::VideoInfo stream_info(video_id)
Gets Video Information

Gets all available Stream information about a video. 

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**video_id** | **String** | The video ID of the YouTube video you want to get information about. | [required] |

### Return type

[**models::VideoInfo**](VideoInfo.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## trending

> Vec<models::StreamItem> trending(region)
Gets all Trending Videos

Gets all Trending Videos in the requested country.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**region** | [**Regions**](.md) | The Region to get trending videos from. | [required] |

### Return type

[**Vec<models::StreamItem>**](StreamItem.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

