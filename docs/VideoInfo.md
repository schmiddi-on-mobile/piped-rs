# VideoInfo

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**audio_streams** | Option<[**Vec<models::Stream>**](Stream.md)> |  | [optional]
**video_streams** | Option<[**Vec<models::Stream>**](Stream.md)> |  | [optional]
**description** | Option<**String**> | The video's description. | [optional]
**dislikes** | Option<**i32**> | The number of dislikes the video has. | [optional]
**duration** | Option<**i32**> | The video's duration in seconds. | [optional]
**hls** | Option<**String**> | The stream of the video in a HLS manifest. | [optional]
**lbry_id** | Option<**String**> | The LBRY ID of the video. | [optional]
**likes** | Option<**i32**> | The number of likes the video has. | [optional]
**livestream** | Option<**bool**> | Whether the video is a livestream. | [optional]
**proxy_url** | Option<**String**> | The base URL of the backend instance's proxy. | [optional]
**subtitles** | Option<[**Vec<models::Subtitle>**](Subtitle.md)> |  | [optional]
**dash** | Option<**String**> | The URL of the DASH manifest. | [optional]
**thumbnail_url** | Option<**String**> | The URL of the video's thumbnail. | [optional]
**title** | Option<**String**> | The video's title. | [optional]
**upload_date** | Option<**String**> | The date the video was uploaded. | [optional]
**uploader** | Option<**String**> | The video's uploader. | [optional]
**uploader_avatar** | Option<**String**> | The URL of the video's uploader's avatar. | [optional]
**uploader_url** | Option<**String**> | The relative URL of the video's uploader. | [optional]
**uploader_verified** | Option<**bool**> | Whether the video's uploader is verified. | [optional]
**views** | Option<**i32**> | The number of views the video has. | [optional]
**related_streams** | Option<[**Vec<models::StreamItem>**](StreamItem.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


