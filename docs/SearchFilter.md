# SearchFilter

## Enum Variants

| Name | Value |
|---- | -----|
| All | all |
| Videos | videos |
| Channels | channels |
| Playlists | playlists |
| MusicSongs | music_songs |
| MusicVideos | music_videos |
| MusicAlbums | music_albums |
| MusicPlaylists | music_playlists |
| MusicArtists | music_artists |


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


