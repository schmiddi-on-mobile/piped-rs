# StreamItem

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**r#type** | Option<**String**> | The type of the stream item. Always stream. | [optional]
**duration** | **i32** | The duration of the video in seconds. | 
**thumbnail** | **String** | The thumbnail of the video. | 
**title** | **String** | The title of the video. | 
**uploaded** | Option<**i64**> | The date in unix epoch the video was uploaded. | [optional]
**uploaded_date** | Option<**String**> | The relative date the video was uploaded on. | [optional]
**uploader_avatar** | Option<**String**> | The avatar of the channel of the video. | [optional]
**uploader_name** | Option<**String**> | The name of the channel of the video. | [optional]
**uploader_url** | Option<**String**> | The relative URL of the channel of the video. | [optional]
**uploader_verified** | Option<**bool**> | Whether or not the channel has a verified badge. | [optional]
**url** | **String** | The relative URL to the video. | 
**views** | Option<**i32**> | The number of views the video has. | [optional]
**is_short** | Option<**bool**> | Whether or not the video is a short video. | [optional]
**short_description** | Option<**String**> | The short description of the video. | [optional]
**content_length** | Option<**i32**> | The content length of the video. | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


