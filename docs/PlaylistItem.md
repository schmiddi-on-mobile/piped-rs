# PlaylistItem

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**r#type** | Option<**String**> | The type of the playlist item. Always playlist. | [optional]
**name** | Option<**String**> | The name of the playlist. | [optional]
**thumbnail** | Option<**String**> | The thumbnail of the playlist. | [optional]
**url** | Option<**String**> | The relative URL of the playlist. | [optional]
**videos** | Option<**i32**> | The number of videos in the playlist. | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


